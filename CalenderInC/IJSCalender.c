
#include<stdio.h>
#include<time.h>
int get_start_day(int year){
	int day;
	day=(((year-1)*365)+((year-1)/4)-((year-1)/100) +((year-1)/400) +1)%7;
	return day;
}
int main(){	
	int year,dayofmonth,weekday=0,startingday;
	printf("\t\t\t**********************************************\n");
	printf("\t\t\t*             WELCOME  TO THE LIST OF        *\n");
	printf("\t\t\t*                  IJS Calendars             *\n");
	printf("\t\t\t**********************************************\n");
	
	printf("enter the year to see its calendar\n");
	scanf("%d",&year);
	
	time_t current_time;//time_t is data type defined for storing time values.
	char* display_time;
	//time(null) returns the current time
	current_time=time(NULL);
	display_time=ctime(&current_time);//ctime() returns the string containing time arguments.
	printf("\n Today is %s",display_time);
	
	char *month[]={"JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER",
	"OCTOBER","NOVEMBER","DECEMBER"};
	int monthdays[]={31,28,31,30,31,30,31,31,30,31,30,31};
	if(year%4==0)
		monthdays[1]=29;
	startingday=get_start_day(year);
	int monthno;
	for(monthno=0;monthno<12;monthno++){
		dayofmonth=monthdays[monthno];
		printf("\n\n*********%s*********\n",month[monthno]);
		printf("\n  Sun  Mon  Tue  Wed  Thur  Fri  Sat\n");
		
		int day;
		for(weekday=0;weekday<startingday;weekday++){
			printf("     ");
		}
		for(day=1;day<=dayofmonth;day++){
			printf("%5d",day);
			if(++weekday>6){
				printf("\n");
				weekday=0;
			}
			//printf("\n\nweekday is%d\n\n",weekday);
			startingday=weekday;
			//printf("starting day is%d",startingday);
		}
	}
	return 0;
}
